import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { InventoryRoutingModule } from './inventory-routing.module';
import { InventoryHomeComponent } from './inventory-home/inventory-home.component';
import { InventoryListComponent } from './inventory-list/inventory-list.component';
import { ViewItemComponent } from './view-item/view-item.component';
import { AddItemComponent } from './add-item/add-item.component';
import { MaterialModule } from '../material/material.module';



@NgModule({
  declarations: [InventoryHomeComponent, InventoryListComponent, ViewItemComponent, AddItemComponent],
  imports: [
    CommonModule,
    InventoryRoutingModule,
    MaterialModule  
  ]
})
export class InventoryModule { }
