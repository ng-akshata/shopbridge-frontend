import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';
import { InventoryListComponent } from './inventory-list/inventory-list.component';
import { ViewItemComponent } from './view-item/view-item.component';
import { AddItemComponent } from './add-item/add-item.component';
import { InventoryHomeComponent } from './inventory-home/inventory-home.component';


const routes: Routes = [
  {
    path: '',
    redirectTo: 'inventory/inventory_list',
    pathMatch: 'full',
  },
  {
    path: 'inventory',
    component: InventoryHomeComponent,
    children:[{
      path: 'add_item',
      component: AddItemComponent,
      
    },
    {
      path: 'view_item',
      component: ViewItemComponent,
      
    },
    {
      path: 'inventory_list',
      component: InventoryListComponent,
      
    }]
  }, 
  
];

@NgModule({
  declarations: [],
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule] 
})
export class InventoryRoutingModule { }
