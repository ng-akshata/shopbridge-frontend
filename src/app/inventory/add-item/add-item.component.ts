import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { InventoryService } from 'src/app/service/inventory.service';
import { Router } from '@angular/router';
import Swal from 'sweetalert2';


@Component({
  selector: 'app-add-item',
  templateUrl: './add-item.component.html',
  styleUrls: ['./add-item.component.css']
})
export class AddItemComponent implements OnInit {


  addItemForm: FormGroup;

  imgURL: any
  disabled: boolean;


  constructor(private fb: FormBuilder,
    private inventorySer: InventoryService, private router: Router) {
  }



  ngOnInit() {
    this.addItemForm = this.fb.group({
      name: ['', Validators.required],
      image: [''],
      description: ['', Validators.required],
      price: ['', Validators.required],
    });
  }
//====================add image ====================================
  onChangeimg(files) {
    console.log(files);
    if (files.length === 0) {
      return;
    }
    const mimeType = files[0].type;
    if (mimeType.match(/image\/*/) == null) {
      console.log('Only images are supported.');
      return;
    }
    this.addItemForm.patchValue({
      image: files[0]
    });
    console.log(this.addItemForm.value);
    const reader = new FileReader();
    reader.readAsDataURL(files[0]);
    reader.onload = (_event) => {
      this.imgURL = reader.result;
    };
  }
  // ===================================================================

  // =================add item ============================================

  addItem() {
    console.log("Item data:=>", this.addItemForm.value);
    if (this.addItemForm.valid) {
      this.inventorySer.addItem(this.addItemForm.value).subscribe(data => {
        console.log(data);
        if (data['success']) {
          this.disabled = true
          Swal.fire("Done", "Item added successfully", "success")
          this.router.navigate(['/inventory/inventory_list'])

        } else {
          Swal.fire("Item already Exist", "", "warning")
        }
      })
    }
    else {
      Swal.fire("Warning", "Fill the form correct", "warning");
    }
  }
  // ================================================================================
}

