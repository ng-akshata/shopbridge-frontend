import { Component, OnInit, ViewChild } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { InventoryService } from '../../service/inventory.service'
import { Router } from '@angular/router';
import Swal from 'sweetalert2';


@Component({
  selector: 'app-inventory-list',
  templateUrl: './inventory-list.component.html',
  styleUrls: ['./inventory-list.component.css']
})
export class InventoryListComponent implements OnInit {
  //============================matTable=========================================== 
  sortedData = [];
  isAsc = true;
  displayedColumns = ['id', 'name', 'price', 'image', 'action'];
  dataSource: MatTableDataSource<any>;
  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;
  @ViewChild(MatSort, { static: false }) sort: MatSort;
  // ================================================================================
  constructor(private invetoryService: InventoryService, private router: Router) {

    this.getAllItem()
  }
  // ============================= View Item in inventory ===============
  viewItem(rowData): void {
    console.log(rowData)
    this.invetoryService.viewObj = rowData
    this.router.navigate(['/inventory/view_item'])
  }
  // =====================================================================
  ngOnInit() {
  }
  // ============================= Get all Item in inventory ===============

  getAllItem() {
    this.invetoryService.getAllItem().subscribe(data => {
      console.log("Order Data=>", data);
      if (data['success']) {
        this.dataSource = new MatTableDataSource(data['data']);
        this.sortedData = data['data']

        this.dataSource.paginator = this.paginator;
        this.dataSource.sort = this.sort;
      } else {
        console.log("not available");

      }
    })
  }

  // ============================= delete Item in inventory ===============
  deleteItem(id) {
    Swal.fire({
      title: 'Are you sure?',
      text: "You Want to Delete Item?",
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Yes!'
    }).then((result) => {
      if (result.value) {
        let obj = {}
        this.invetoryService.deleteItem(id).subscribe(data => {
          console.log(data);
          if (data['success']) {
            this.getAllItem()
            Swal.fire("Done", "Item Deleted Successfully", "success")
          }

        })


      }

    })
  }
  // ============================================================================

  //============================ apply filter===========================================
  applyFilter(filterValue: string) {
    filterValue = filterValue.trim(); // Remove whitespace
    filterValue = filterValue.toLowerCase(); // Datasource defaults to lowercase matches
    this.dataSource.filter = filterValue;
  }
}