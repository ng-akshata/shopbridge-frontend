import { Component, OnInit } from '@angular/core';
import { InventoryService } from 'src/app/service/inventory.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-view-item',
  templateUrl: './view-item.component.html',
  styleUrls: ['./view-item.component.css']
})
export class ViewItemComponent implements OnInit {
itemData:any
  constructor(private inventoryService: InventoryService, private router :Router) { 
    if(inventoryService.viewObj==undefined){
      router.navigate(['/inventory/inventory_list'])
    }else{
    console.log(inventoryService.viewObj);
    this.itemData=inventoryService.viewObj
    }
  }

  ngOnInit() {
  }

}
