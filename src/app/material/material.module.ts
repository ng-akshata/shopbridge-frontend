
import { NgModule } from "@angular/core";

import {MatChipsModule} from '@angular/material/chips';
import {MatSidenavModule} from '@angular/material/sidenav';
import {MatDialogModule} from '@angular/material/dialog';
import {MatSortModule} from '@angular/material/sort';
import {MatCardModule} from '@angular/material/card';
import {MatTabsModule} from '@angular/material/tabs';
import {MatTableModule} from '@angular/material//table';
import {MatIconModule} from '@angular/material/icon';
import {MatPaginatorModule} from '@angular/material/paginator';
import {MatFormFieldModule} from '@angular/material/form-field';
import {MatSelectModule} from '@angular/material/select';
import {MatInputModule} from '@angular/material/input';
import {MatButtonModule} from '@angular/material/button';
import {MatDividerModule} from '@angular/material/divider';
import {MatGridListModule} from '@angular/material/grid-list';
import {MatListModule} from '@angular/material/list';
import {MatCheckboxModule} from '@angular/material/checkbox';
import {MatDatepickerModule} from '@angular/material/datepicker';
import {MatMenuModule} from '@angular/material/menu';
import {MatRadioModule} from '@angular/material/radio';
import {MatExpansionModule} from '@angular/material/expansion';
import {MatTooltipModule} from '@angular/material/tooltip';
import {MatSlideToggleModule} from '@angular/material/slide-toggle';
import {MatStepperModule} from '@angular/material/stepper';
import {MatAutocompleteModule} from '@angular/material/autocomplete';
import {MatToolbarModule} from '@angular/material/toolbar';
import {MatBottomSheetModule} from '@angular/material/bottom-sheet';
import { DragDropModule } from '@angular/cdk/drag-drop';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { MatSliderModule } from '@angular/material/slider';
import { MatBadgeModule } from '@angular/material/badge';
@NgModule({
  imports: [
    MatSidenavModule,
    MatDialogModule,
    MatSortModule,
    MatCardModule,
    MatTabsModule,
    MatTableModule,
    MatIconModule,
    MatPaginatorModule,
    MatFormFieldModule,
    MatSelectModule,
    MatInputModule,
    MatButtonModule,
    MatDividerModule,
    MatGridListModule,
    MatListModule,
    MatCheckboxModule,
    MatDatepickerModule,
    MatMenuModule,
    MatRadioModule,
    MatExpansionModule,
    MatTooltipModule,
    MatSlideToggleModule,
    DragDropModule,
    MatStepperModule,
    MatAutocompleteModule,
    ReactiveFormsModule,
    MatSliderModule,
    MatToolbarModule,
    MatBottomSheetModule,
    FormsModule,
    MatBadgeModule,
    MatChipsModule,
    MatIconModule
  ],
  exports: [
    MatSidenavModule,
    MatDialogModule,
    MatSortModule,
    MatCardModule,
    MatTabsModule,
    MatTableModule,
    MatIconModule,
    MatPaginatorModule,
    MatFormFieldModule,
    MatSelectModule,
    MatInputModule,
    MatButtonModule,
    MatDividerModule,
    MatGridListModule,
    MatListModule,
    MatCheckboxModule,
    MatDatepickerModule,
    MatRadioModule,
    MatExpansionModule,
    MatTooltipModule,
    MatSlideToggleModule,
    DragDropModule,
    MatStepperModule,
    MatAutocompleteModule,
    ReactiveFormsModule,
    MatSliderModule,
    MatToolbarModule,
    MatBottomSheetModule,
    FormsModule,
    MatBadgeModule,
    MatChipsModule,
    MatIconModule
  ],
  declarations: [],
})
export class MaterialModule { }
