import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../environments/environment';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class InventoryService {

  constructor(private http: HttpClient) { }
  viewObj: any
  //===================================add item===========================
  addItem(obj): Observable<any> {
    console.log(obj);
    const formData: any = new FormData();
    formData.append('image', obj.image);
    Object.keys(obj).forEach(ele => {
      if (ele !== 'image') {
        formData.append(ele, obj[ele]);
      }
    });
    return this.http.post(environment.base_url + 'inventory/create-item', formData)
  }

  // ========================= get all item =================================
  getAllItem() {
    return this.http.get(environment.base_url + 'inventory/get-item-list')
  }

  // ========================= delete perticular item =================================
  deleteItem(id) {
    return this.http.put(environment.base_url + 'inventory/delete-item/' + id, {})
  }
}
